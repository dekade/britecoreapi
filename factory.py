# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api
from models import RiskType
from flask_cors import CORS


def create_tables():
    if not RiskType.exists():
        RiskType.create_table(wait=True)
        RiskType.load("example_risktypes.json")


def create_api():
    create_tables()
    app = Flask(__name__)
    CORS(app, resources={r"/api/*": {"origins": "*"}})
    api = Api(app)
    return api


