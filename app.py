# -*- coding: utf-8 -*-
from flask_restful.representations import json
from factory import create_api
from flask_restful import Resource
from models import RiskType, ModelEncoder

api = create_api()
app = api.app


class Home(Resource):
    def get(self):
        return "RiskTypeApi"


class RiskTypeApi(Resource):
    def get(self, company):
        with app.app_context():
            response = []
            if company:
                result = RiskType.query(company)
                response = [{'name': item.name, 'description': item.description} for item in result]
            return response


class RiskTypeByIdApi(Resource):
    def get(self, company, name):
        response = None
        if company and name:
            result = list(RiskType.query(company, RiskType.name == name, limit=1))
            if result:
                with app.app_context():
                    response = json.make_response(json.dumps(result[0], cls=ModelEncoder))
        return response


api.add_resource(Home, '/')
api.add_resource(RiskTypeApi, '/api/risk_types/<company>', endpoint='risk_types')
api.add_resource(RiskTypeByIdApi, '/api/risk_types/<company>/<name>', endpoint='risk_types_by_name')

if __name__ == '__main__':
    app.run(debug=True)
    pass
