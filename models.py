# -*- coding: utf-8 -*-
import decimal
from datetime import datetime
from enum import Enum
from flask import json
from pynamodb.attributes import UnicodeAttribute, ListAttribute, MapAttribute, BooleanAttribute
from pynamodb.constants import STRING
from pynamodb.models import Model

DATA_TYPES = {'integer': int, 'string': str, 'boolean': bool, 'decimal': decimal, 'datetime': datetime, 'enum': Enum}


class DataTypeAttribute(UnicodeAttribute):
    attr_type = STRING

    def serialize(self, value):
        if value not in DATA_TYPES.keys():
            raise ValueError("{attr_name}s must be one of {DATA_TYPES}s, not '{value}s'".format(
                DATA_TYPES=DATA_TYPES.keys(), attr_name=self.attr_name, value=value))
        else:
            return UnicodeAttribute.serialize(self, value)


class Field(MapAttribute):
    name = UnicodeAttribute()
    description = UnicodeAttribute(null=True)
    data_type = DataTypeAttribute(null=False)
    is_required = BooleanAttribute(default=True)
    data = ListAttribute(default=[])


class RiskType(Model):
    class Meta:
        table_name = "britecore-api-risktype"
        region = 'eu-west-2'
        write_capacity_units = 1
        read_capacity_units = 2

    company = UnicodeAttribute(hash_key=True)
    name = UnicodeAttribute(range_key=True)
    description = UnicodeAttribute(null=True)
    fields = ListAttribute(default=[])


class ModelEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'attribute_values'):
            return obj.attribute_values
        elif isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)
