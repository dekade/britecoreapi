# -*- coding: utf-8 -*-
import unittest
from app import Home, RiskTypeApi, RiskTypeByIdApi
from mock import patch, mock


class HomeTest(unittest.TestCase):

    def test_get(self):
        resource = Home()
        result = resource.get()
        self.assertEqual("RiskTypeApi", result)


class RiskTypeApiTest(unittest.TestCase):
    def setUp(self):
        self.ExampleItem = mock.Mock()
        self.ExampleItem.name = "TEST name"
        self.ExampleItem.description = "TEST description"

    @patch('models.RiskType.query')
    def test_get_no_argument(self, mock_query):
        mock_query.return_value = []
        resource = RiskTypeApi()

        result = resource.get('')
        self.assertFalse(mock_query.called)
        self.assertEqual([], result)

    @patch('models.RiskType.query')
    def test_get_unsuccessful(self, mock_query):
        mock_query.return_value = []
        resource = RiskTypeApi()

        result = resource.get("COMPANY")
        mock_query.assert_called_with("COMPANY")
        self.assertEqual([], result)

    @patch('models.RiskType.query')
    def test_get_successful(self, mock_query):
        mock_query.return_value = [self.ExampleItem]
        resource = RiskTypeApi()

        result = resource.get("COMPANY")
        mock_query.assert_called_with("COMPANY")
        self.assertEqual([{'name': 'TEST name', 'description':'TEST description'}], result)


class RiskTypeByIdApiTest(unittest.TestCase):
    @patch('models.RiskType.query')
    def test_get_no_args(self, mock_query):
        mock_query.return_value = []
        resource = RiskTypeByIdApi()
        result = resource.get('','')
        self.assertFalse(mock_query.called)
        self.assertEqual(None, result)

    @patch('models.RiskType.query')
    def test_get_unsuccessful(self, mock_query):
        mock_query.return_value = []
        resource = RiskTypeByIdApi()
        result = resource.get("COMPANY", "RISK TYPE NAME")
        self.assertTrue(mock_query.called)
        self.assertEqual(None, result)

    @patch('models.RiskType.query')
    def test_get_successful(self, mock_query):
        mock_query.return_value = [{'name': 'RISK TYPE NAME'}]
        resource = RiskTypeByIdApi()
        result = resource.get("COMPANY", "RISK TYPE NAME")
        self.assertTrue(mock_query.called)
        self.assertEqual('{"name": "RISK TYPE NAME"}', result.data)


if __name__ == "__main__":
    unittest.main()
