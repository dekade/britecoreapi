# -*- coding: utf-8 -*-
import unittest
from models import Field, RiskType, DataTypeAttribute
from pynamodb.exceptions import TableError
from factory import create_tables


class RiskTypeTest(unittest.TestCase):
    def setUp(self):
        RiskType.Meta.host = 'http://localhost:8000'
        RiskType.Meta.table_name = "britecore-api-risktype-test"

        if not RiskType.exists():
            RiskType.create_table(wait=True)

        create_tables()
        try:
            RiskType.delete_table()
        except TableError:
            pass

    def test_risktype_save_count(self):
        r = RiskType()
        r.company = "ACME"
        r.name = "Test Risk Type"
        r.save()
        result = RiskType.count('ACME', RiskType.name == 'Test Risk Type')
        self.assertEqual(1, result)

    def test_risktype_save_get_by_name(self):
        r = RiskType()
        r.company = "ACME"
        r.name = "Test Risk Type"
        r.save()
        result = RiskType.get('ACME', 'Test Risk Type')
        self.assertEqual("Test Risk Type", result.name)

    def test_risktype_save_all_fieldtypes(self):
        expected = """[["ACME", {"attributes": {"fields": {"L": [{"M": {"is_required": {"BOOL": true}, "data": {"L": [{"S": "yes"}, {"S": "no"}, {"S": "maybe"}, {"S": "unsure"}]}, "name": {"S": "Enum test field"}, "data_type": {"S": "enum"}, "description": {"S": "Enum test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Boolean test field"}, "data_type": {"S": "boolean"}, "description": {"S": "Boolean test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Integer test field"}, "data_type": {"S": "integer"}, "description": {"S": "Integer test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Decimal test field"}, "data_type": {"S": "decimal"}, "description": {"S": "Decimal test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Datetime test field"}, "data_type": {"S": "datetime"}, "description": {"S": "Datetime test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "String test field"}, "data_type": {"S": "string"}, "description": {"S": "String test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": [{"S": "yes"}, {"S": "no"}, {"S": "maybe"}, {"S": "unsure"}]}, "name": {"S": "Required Enum test field"}, "data_type": {"S": "enum"}, "description": {"S": "Required Enum test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Required Boolean test field"}, "data_type": {"S": "boolean"}, "description": {"S": "Required Boolean test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Required Integer test field"}, "data_type": {"S": "integer"}, "description": {"S": "Required Integer test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Required Decimal test field"}, "data_type": {"S": "decimal"}, "description": {"S": "Required Decimal test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "RequiredDatetime test field"}, "data_type": {"S": "datetime"}, "description": {"S": "Required Datetime test field description."}}}, {"M": {"is_required": {"BOOL": true}, "data": {"L": []}, "name": {"S": "Required String test field"}, "data_type": {"S": "string"}, "description": {"S": "Required String test field description."}}}]}, "description": {"S": "Example risk type with all field types"}}, "range_key": "All fields showcase"}]]"""
        r = RiskType()
        r.company = "ACME"
        r.name = "All fields showcase"
        r.description = "Example risk type with all field types"

        f = Field()
        f.name = "Enum test field"
        f.description = "Enum test field description."
        f.data_type = "enum"
        f.data = ["yes", "no", "maybe", "unsure"]
        r.fields.append(f)

        f1 = Field()
        f1.name = "Boolean test field"
        f1.description = "Boolean test field description."
        f1.data_type = "boolean"
        r.fields.append(f1)

        f2 = Field()
        f2.name = "Integer test field"
        f2.description = "Integer test field description."
        f2.data_type = "integer"
        r.fields.append(f2)

        f3 = Field()
        f3.name = "Decimal test field"
        f3.description = "Decimal test field description."
        f3.data_type = "decimal"
        r.fields.append(f3)

        f4 = Field()
        f4.name = "Datetime test field"
        f4.description = "Datetime test field description."
        f4.data_type = "datetime"
        r.fields.append(f4)

        f5 = Field()
        f5.name = "String test field"
        f5.description = "String test field description."
        f5.data_type = "string"
        r.fields.append(f5)

        f6 = Field()
        f6.name = "Required Enum test field"
        f6.description = "Required Enum test field description."
        f6.data_type = "enum"
        f6.data = ["yes", "no", "maybe", "unsure"]
        f6.is_required = True
        r.fields.append(f6)

        f7 = Field()
        f7.name = "Required Boolean test field"
        f7.description = "Required Boolean test field description."
        f7.data_type = "boolean"
        f7.is_required = True
        r.fields.append(f7)

        f8 = Field()
        f8.name = "Required Integer test field"
        f8.description = "Required Integer test field description."
        f8.data_type = "integer"
        f8.is_required = True
        r.fields.append(f8)

        f9 = Field()
        f9.name = "Required Decimal test field"
        f9.description = "Required Decimal test field description."
        f9.data_type = "decimal"
        f9.is_required = True
        r.fields.append(f9)

        f10 = Field()
        f10.name = "RequiredDatetime test field"
        f10.description = "Required Datetime test field description."
        f10.data_type = "datetime"
        f10.is_required = True
        r.fields.append(f10)

        f11 = Field()
        f11.name = "Required String test field"
        f11.description = "Required String test field description."
        f11.data_type = "string"
        f11.is_required = True
        r.fields.append(f11)

        r.save()
        result = r.dumps()
        self.assertEqual(expected, result)



if __name__ == "__main__":
    unittest.main()

